﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consts 
{
    public const string Rocket = "Rocket";
    public const string Bullet = "Bullet";

    public const string RocketTag = "Rocket";
    public const string BulletTag = "Bullet";
    public const string WallTag   = "Wall";

}
