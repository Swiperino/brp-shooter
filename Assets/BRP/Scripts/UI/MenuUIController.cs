﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUIController : Singleton<MenuUIController>
{

    [SerializeField] private Transform _mainPnl;
    [SerializeField] private Transform _gameModePnl;
    [SerializeField] private Transform _inRoomPnl;

    private void Start()
    {
        TogglePanels(_mainPnl);
    }

    private void TogglePanels(Transform currentPanel)
    {
        currentPanel.SetAsLastSibling();
    }

    #region Buttons

    public void ShowMainPnl()
    {
        TogglePanels(_mainPnl);
    }

    public void ShowGameModePnl()
    {
        TogglePanels(_gameModePnl);
    }

    public void ShowInRoomPnl()
    {
        TogglePanels(_inRoomPnl);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    #endregion Buttons
	
}
