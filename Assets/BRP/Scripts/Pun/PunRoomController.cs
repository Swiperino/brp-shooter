﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PunRoomController : MonoBehaviour
{

    #region HostGame

    public void JoinRandomGame()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    private void OnPhotonRandomJoinFailed()
    {
        Debug.Log("[Photon] OnPhotonRandomJoinFailed.");
        Create();
    }

    private void Create()
    {
        var room = SetRoomOptions(2);
        PhotonNetwork.CreateRoom(null, room, TypedLobby.Default);
    }

    private RoomOptions SetRoomOptions(byte players)
    {
        var roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = players;
        roomOptions.IsVisible = true;
        roomOptions.IsOpen = true;

        return roomOptions;
    }

    #endregion

    #region JoinRoom

    private void OnJoinedRoom()
    {
        MenuUIController.I.ShowInRoomPnl();
        Debug.Log("[Photon] OnJoinedRoom.");
        CheckGameStart();
    }

    #endregion

    #region LeaveRoom

    public void LeaveCurrentGame()
    {
        PhotonNetwork.LeaveRoom();
        Debug.Log("[Photon] LeaveRoom.");
    }

    private void OnLeftRoom()
    {
        MenuUIController.I.ShowGameModePnl();
        Debug.Log("[Photon] OnLeftRoom.");
    }

    #endregion

    #region StartGame

    private void CheckGameStart()
    {
        if (PhotonNetwork.room.MaxPlayers == PhotonNetwork.playerList.Length)
        {
            StartGame();
        }
    }

    private void StartGame()
    {
        var opt = RaiseEventOptions.Default;
        opt.Receivers = ReceiverGroup.MasterClient;
        PunRaiseEventHelper.PunRaiseOperation(OperationByte.StartGame, opt);
    }

    #endregion

}





