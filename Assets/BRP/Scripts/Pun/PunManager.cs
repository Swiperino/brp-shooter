﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PunManager : MonoBehaviour
{
    
    private void Start () 
    {
        PhotonNetwork.automaticallySyncScene = true;
        PhotonNetwork.ConnectUsingSettings("1");
	}

    private void OnConnectedToMaster()
    {
        Debug.Log("[Photon] Connected to master.");
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }
	
    private void OnJoinedLobby()
    {
        Debug.Log("[Photon] Joined lobby.");
    }
} 
