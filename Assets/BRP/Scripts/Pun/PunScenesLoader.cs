﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunScenesLoader : MonoBehaviour 
{
    
    private void OnEnable()
    {
        PhotonNetwork.OnEventCall += this.OnEvent;
    }
    private void OnDisable()
    {
        PhotonNetwork.OnEventCall -= this.OnEvent;
    }

    private void OnEvent(byte eventcode, object content, int senderid)
    {
        
        switch (eventcode)
        {
            case OperationByte.StartGame:
                LoadScene(Scenes.Game);
            break;
        }
    }

    private void LoadScene(Scenes currentScene)
    {
        if (PhotonNetwork.isMasterClient)
            PhotonNetwork.LoadLevel((int)currentScene);
    }
}
