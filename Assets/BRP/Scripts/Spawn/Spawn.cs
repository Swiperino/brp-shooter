﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SpawnData
{
    public List<Spawn> Spawns = new List<Spawn>();
}

[Serializable]
public class Spawn
{
    public enum SpawnState
    {
        Empty, Full
    }

    public SpawnState CurrentState = SpawnState.Empty;
    public Vector2 Position;
}

