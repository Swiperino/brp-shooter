﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnsController : Photon.MonoBehaviour
{
    [SerializeField]
    private SpawnData _spawnData = new SpawnData();


    private void Start()
    {
        SetSpawnPositionForAllPlayer();
    }

    private void SetSpawnPositionForAllPlayer()
    {
        if (PhotonNetwork.isMasterClient)
        {
            var players = PhotonNetwork.playerList;

            for (int i = 0; i < players.Length; i++)
            {
                var spawn = GetFreeSpawn();
                var spawnStr = JsonUtility.ToJson(spawn);
                photonView.RPC("SpawnPlayer", players[i], spawnStr);
            }
        }
    }
 
    [PunRPC]
    private void SpawnPlayer(string spawn)
    {
        Spawn sp = JsonUtility.FromJson<Spawn>(spawn);
        PhotonNetwork.Instantiate(Consts.Rocket, sp.Position , Quaternion.identity, 0);
    }


    private Spawn GetFreeSpawn()
    {
        var spawns = _spawnData.Spawns;

        for (int i = 0; i < spawns.Count; i++)
        {
            if (spawns[i].CurrentState == Spawn.SpawnState.Empty)
            {
                spawns[i].CurrentState = Spawn.SpawnState.Full;
                return spawns[i];
            }
        }
        return new Spawn();
    }

}
