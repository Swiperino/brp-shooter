﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PunRaiseEventHelper 
{
    public static void PunRaiseOperation(byte currentOperation)
    {
        PhotonNetwork.RaiseEvent(currentOperation, null, true, null);
    }

    public static void PunRaiseOperation(byte currentOperation, object content)
    {
        PhotonNetwork.RaiseEvent(currentOperation, content, true, null);
    }

    public static void PunRaiseOperation(byte currentOperation, RaiseEventOptions options)
    {
        PhotonNetwork.RaiseEvent(currentOperation, null, true, options);
    }

    public static void PunRaiseOperation(byte currentOperation, object content, RaiseEventOptions options)
    {
        PhotonNetwork.RaiseEvent(currentOperation, content, true, options);
    }
}
