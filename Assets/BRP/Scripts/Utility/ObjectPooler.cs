﻿using UnityEngine;


    public class ObjectPooler<T> where T : Component
    {
        private Vector3 outPosition;
        private T[] pool;

        public ObjectPooler(int amount, T prefab, Transform parent, Vector3 outPosition)
        {
            this.outPosition = outPosition;
            pool = new T[amount];
            for(int i = 0; i < amount; i++)
            {
                T clone = Object.Instantiate(prefab);
                clone.transform.parent = parent;
                clone.transform.localPosition = outPosition;
                clone.transform.localScale = Vector3.one;
                pool[i] = clone;
            }
        }

        public bool IsObjectInPool(T pooledObject)
        {
            return pooledObject.transform.localPosition == outPosition;
        }

        public T GetPooledObject()
        {
            //return pool.First(pooledObject => pooledObject.transform.localPosition == outPosition);

            for(int i = 0; i < pool.Length; i++)
            {
                if(pool[i].transform.localPosition == outPosition)
                    return pool[i];
            }
            return null;
        }

        public void SetPooledObject(T pooledObject)
        {
            pooledObject.transform.localPosition = outPosition;
        }

        public void SetAllPooledObjects()
        {
            for(int i = 0; i < pool.Length; i++)
                pool[i].transform.localPosition = outPosition;
        }
    }
