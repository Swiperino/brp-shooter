﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : Photon.MonoBehaviour
{
    public static event Action RocketWasHit;


    [SerializeField]
    public GameObject _bullet;
    [SerializeField]
    private Transform _spawnBulletPosition;

    private Rigidbody2D _rb2D;

    private float _moveSpeed = 2.5f;
    private float _rotationSpeed = 80;
    private float _yAxis;
    private float _xAxis;


    private void Start()
    {
        _rb2D = GetComponent<Rigidbody2D>();	
    }

    private void Update()
    {
        if (!photonView.isMine)
            return;
        
        _yAxis = Input.GetAxis("Vertical");
        _xAxis = -Input.GetAxis("Horizontal");

        if (Input.GetMouseButtonDown(0))
            SpawnBullet();
    }

    private void SpawnBullet()
    {
        PhotonNetwork.Instantiate(Consts.Bullet, _spawnBulletPosition.position, transform.rotation, 0);
//        Instantiate(_bullet, _spawnBulletPosition.position, transform.rotation);
    }

    private void FixedUpdate()
    {
        if (!photonView.isMine)
            return;

        RotateRocket(_xAxis);
        MoveRocket(_yAxis);
    }


    private void RotateRocket(float amount)
    {
        _rb2D.MoveRotation(_rb2D.rotation + (amount * _rotationSpeed) * Time.fixedDeltaTime);
    }

    private void MoveRocket(float amount)
    {
        Vector2 force = transform.up * amount * _moveSpeed;
        _rb2D.AddForce(force);
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.collider.CompareTag(Consts.BulletTag))
        {
            RocketWasHit.RaiseEvent();
        }
    }
}
