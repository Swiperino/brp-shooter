﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController :Photon.MonoBehaviour 
{
    private Rigidbody2D _rb2D;

    private float _bulletSpeed = 0.1f;
    private float _bulletLifeTime = 3;

    private void Start()
    {
        _rb2D = GetComponent<Rigidbody2D>();
        Invoke("DestroyBullet", _bulletLifeTime);
    }

    private void FixedUpdate()
    {
        BulletMove();
    }

    private void BulletMove()
    {
        Vector2 up = new Vector2(transform.up.x, transform.up.y);
        _rb2D.MovePosition(_rb2D.position + up * _bulletSpeed);

    }

    private void DestroyBullet()
    {
        if(photonView.isMine)
            PhotonNetwork.Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.CompareTag(Consts.WallTag))
        {
            DestroyBullet();
            print("WithWall");

        }
        else if (coll.CompareTag(Consts.RocketTag))
        {
            DestroyBullet();
            print("WithRocket");
        }
    }
}
